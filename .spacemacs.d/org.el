;;
;; Much of this org setup is derived from http://doc.norang.ca/org-mode.html
;; and https://github.com/vedang/org-mode-crate
;; and https://github.com/vedang/emacs-up/blob/master/enhance/org-crate-config.el
;;

;; Setup directory and file paths for org
(setq org-directory "~/Documents/org"
      org-archive-directory (concat org-directory "/archive")
      org-archive-location (concat org-archive-directory "/%s_archive::")
      org-default-notes-file (concat org-directory "/refile.org")
      org-agenda-files (list org-directory))


;; org-todo settings
;; keys mentioned in brackets are hot-keys for the States
;; ! indicates insert timestamp
;; @ indicates insert note
;; / indicates entering the state
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

;; (setq org-todo-keywords
;;       '((sequence "TODO(t!/!)" "WORKING(w!/!)"
;;                   "REDO(R@/!)" "WAITING(a@/!)"
;;                   "|" "DONE(d!/@)" "DELEGATED(e@/!)")
;;         (sequence "PROJECT(p)" "FOLLOWUP(f!/!)" "MAINT(m!/!)"
;;                   "|" "SOMEDAY(s)" "CANCELLED(c@/!)"
;;                   "RESTRUCTURED(r@/!)")))



;; Automatically assign tags to tasks based on state changes
(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING") ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

;; Changing State should trigger following Tag changes
;; (setq org-todo-state-tags-triggers
;;       '(("SOMEDAY"
;;          ("waiting" . t) ("next"))
;;         (done
;;          ("next") ("waiting"))
;;         ("WAITING"
;;          ("next") ("waiting" . t))
;;         ("TODO"
;;          ("waiting"))
;;         ("FOLLOWUP"
;;          ("followup" . t))
;;         ("WORKING"
;;          ("waiting") ("next" . t))))


;; Other todo related settings
(setq org-use-fast-todo-selection t
      org-fast-tag-selection-single-key 'expert
      ;; Allow me to change state without it being logged
      org-treat-S-cursor-todo-selection-as-state-change nil
      ;; show TODO counts of _all_ subtasks under a heading
      org-hierarchical-todo-statistics nil
      org-hierarchical-checkbox-statistics nil
      org-enforce-todo-dependencies t)


;; Capture templates for: TODO tasks, Notes, appointments, phone calls,
;; meetings, and org-protocol
(setq org-capture-templates
      (quote (("t" "todo" entry (file org-default-notes-file)
                "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
              ("r" "respond" entry (file org-default-notes-file)
                "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
              ("n" "note" entry (file org-default-notes-file)
                "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
              ("j" "Journal" entry (file+datetree "~/Documents/org/diary.org")
                "* %?\n%U\n" :clock-in t :clock-resume t)
              ("w" "org-protocol" entry (file org-default-notes-file)
                "* TODO Review %c\n%U\n" :immediate-finish t)
              ("m" "Meeting" entry (file org-default-notes-file)
                "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
              ("p" "Phone call" entry (file org-default-notes-file)
                "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
              ("h" "Habit" entry (file org-default-notes-file)
                "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))


;; refile settings
(setq org-refile-targets '((org-agenda-files :maxlevel . 4)
                           (nil :maxlevel . 4))
      ;; Targets start with the file name - allows creating level 1 tasks
      org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil
      org-refile-allow-creating-parent-nodes 'confirm)



;;org-tags
;; Important Tag list
(setq org-tag-alist '(("next" . ?x)
                      ("release" . ?r)
                      ("bug" . ?b)
                      ("note" . ?N)
                      ("study" . ?s)
                      ("goal" . ?g)
                      ("dp" . ?d)
                      ("tweak" . ?t)
                      ("write" . ?w)
                      ("productive" . ?p)
                      ("essential" . ?e)
                      ("nonessential" . ?n)
                      ("waiting" . ?a)
                      ("future" . ?f)))


;; org-priorities
(setq org-lowest-priority ?E)
(setq org-default-priority ?D)


;; Logbook settings
(setq org-log-done 'time
      org-log-into-drawer t
      org-log-reschedule 'note
      org-log-redeadline 'note)


;; settings for org-clock
(org-clock-persistence-insinuate)
(setq org-clock-history-length 20
      org-clock-in-resume t
      org-drawers '("PROPERTIES" "LOGBOOK" "CLOCK")
      org-clock-into-drawer "CLOCK"
      org-clock-out-remove-zero-time-clocks t
      org-clock-out-when-done t
      org-clock-persist t
      org-clock-persist-file (concat org-directory "/org-clock-save")
      org-clock-auto-clock-resolution 'when-no-clock-is-running
      org-clock-report-include-clocking-task t)


;; List of TODO states to clock-in
(setq vm/todo-list '("TODO" "WAITING" "REDO"))


;; Change task state to WORKING when clocking in
(defun bh/clock-in-to-working (kw)
    "Switch task from TODO to WORKING when clocking in.
Skips capture tasks and tasks with subtasks"
    (when (and (not (and (boundp 'org-capture-mode) org-capture-mode))
               (member kw vm/todo-list))
      "WORKING"))

(setq org-clock-in-switch-to-state 'bh/clock-in-to-working)


;; Remove empty LOGBOOK drawers on clock out
(defun bh/remove-empty-drawer-on-clock-out ()
  (interactive)
  (save-excursion
    (beginning-of-line 0)
    (org-remove-empty-drawer-at "LOGBOOK" (point))))

(add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)


(setq bh/keep-clock-running nil)


(defun bh/clock-in-last-task (arg)
    "Clock in the interrupted task if there is one
Skip the default task and get the next one.
A prefix arg forces clock in of the default task."
    (interactive "p")
    (let ((clock-in-to-task
           (cond
            ((eq arg 4) org-clock-default-task)
            ((and (org-clock-is-active)
                                  (equal org-clock-default-task (cadr
                                                                 org-clock-history)))
             (caddr org-clock-history))
            ((org-clock-is-active) (cadr org-clock-history))
                      ((equal org-clock-default-task (car org-clock-history))
                       (cadr org-clock-history))
                      (t (car org-clock-history)))))
      (org-with-point-at clock-in-to-task
        (org-clock-in nil))))
(spacemacs/set-leader-keys "ol" 'bh/clock-in-last-task)


(defun bh/punch-in (arg)
    "Start continuous clocking and set the default task to the
selected task.  If no task is selected set the Organization task
as the default task."
    (interactive "p")
    (setq bh/keep-clock-running t)
    (if (equal major-mode 'org-agenda-mode)
        ;;
        ;; We're in the agenda
        ;;
        (let* ((marker (org-get-at-bol 'org-hd-marker))
               (tags (org-with-point-at marker (org-get-tags-at))))
          (if (and (eq arg 4) tags)
              (org-agenda-clock-in '(16))
            (bh/clock-in-organization-task-as-default)))
      ;;
      ;; We are not in the agenda
      ;;
      (save-restriction
        (widen)
        ;; Find the tags on the current task
        (if (and (equal major-mode 'org-mode)
                 (not (org-before-first-heading-p))
                 (eq arg 4))
            (org-clock-in '(16))
          (bh/clock-in-organization-task-as-default)))))
(spacemacs/set-leader-keys "oi" 'bh/punch-in)


(defun bh/punch-out ()
  (interactive)
  (setq bh/keep-clock-running nil)
  (when (org-clock-is-active)
    (org-clock-out))
  (org-agenda-remove-restriction-lock))
(spacemacs/set-leader-keys "oo" 'bh/punch-out)


(defun bh/clock-in-default-task ()
  (save-excursion
    (org-with-point-at org-clock-default-task
      (org-clock-in))))


(defun bh/clock-in-parent-task ()
  "Move point to the parent (project) task if any and clock in"
  (let ((parent-task))
    (save-excursion
      (save-restriction
        (widen)
        (while (and (not parent-task) (org-up-heading-safe))
                    (when (member (nth 2 (org-heading-components))
                                  org-todo-keywords-1)
                      (setq parent-task (point))))
        (if parent-task
            (org-with-point-at parent-task
              (org-clock-in))
          (when bh/keep-clock-running
            (bh/clock-in-default-task)))))))


(defun bh/clock-in-organization-task-as-default ()
  (interactive)
  (when (boundp 'bh/organization-task-id)
    (org-with-point-at (org-id-find bh/organization-task-id 'marker)
      (org-clock-in '(16)))))


(defun bh/clock-out-maybe ()
  (when (and bh/keep-clock-running
             (not org-clock-clocking-in)
             (marker-buffer org-clock-default-task)
             (not org-clock-resolving-clocks-due-to-idleness))
    (bh/clock-in-parent-task)))


(add-hook 'org-clock-out-hook 'bh/clock-out-maybe 'append)
