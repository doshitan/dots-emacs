(setq pollen-packages
  '(
    ;; (pollen :location (recipe
    ;;                    :fetcher github
    ;;                    :repo "basus/pollen-mode"))
    (pollen :location local)
    ))

(defun pollen/init-pollen ()
  (use-package pollen
    :defer t
    :mode ("\\.p\\'" . pollen-mode)
    :mode ("\\.pp\\'" . pollen-mode)
    :mode ("\\.pm\\'" . pollen-mode)
    :mode ("\\.pmd\\'" . pollen-mode)
    :mode ("\\.ptree\\'" . pollen-mode)
    :init
    (progn
      (defun spacemacs/pollen-insert-and-switch-lozenge ()
        "Call `pollen-insert-lozenge' and enable `insert state'."
        (interactive)
        (pollen-insert-lozenge)
        (evil-insert-state))

      (spacemacs/set-leader-keys-for-major-mode 'pollen-mode
        "il" 'spacemacs/pollen-insert-and-switch-lozenge)
      )))
