;; -*- mode: emacs-lisp -*-
;; https://github.com/syl20bnr/spacemacs/blob/master/core/templates/.spacemacs.template
;; https://gitlab.com/travisbhartwell/vcsh_emacs

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values"
  (setq-default
   dotspacemacs-editing-style 'vim
   dotspacemacs-themes '(zenburn leuven)
   dotspacemacs-command-key ";"
   ;; these are the default, but gotta specify them to make the dotfile tests happy
   dotspacemacs-leader-key "SPC"
   dotspacemacs-emacs-leader-key "M-m"
  )
)

(defun dotspacemacs/layers ()
  "Configuration Layers declaration
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   dotspacemacs-configuration-layer-path '("~/.spacemacs.d/layers/")
   dotspacemacs-configuration-layers
   '(
    ;; ----------------------------------------------------------------
    ;; Add/change some layer names and press <SPC f e R> (Vim style) or
    ;; <M-m f e R> (Emacs style) to install them.
    ;; ----------------------------------------------------------------
    (auto-completion :variables
                     auto-completion-enable-help-tooltip t
                     auto-completion-enable-sort-by-usage t)
    colors
    elixir
    (elm :variables
         elm-format-command "elm-format-0.17"
         elm-format-on-save t
         elm-sort-imports-on-save t)
    emacs-lisp
    git
    (haskell :variables
             haskell-enable-ghc-mod-support t
             haskell-process-type 'cabal-repl)
    html
    javascript
    latex
    markdown
    nixos
    org
    php
    pollen
    racket
    (shell :variables
           shell-default-height 30
           shell-default-position 'bottom
           shell-default-term-shell (getenv "SHELL"))
    spell-checking
    syntax-checking
    systemd
    ; themes-megapack ; organic-green moe-dark sanityinc-tomorrow-night sanityinc-tomorrow-eighties
    version-control
   )
  dotspacemacs-additional-packages
  '(
    yaml-mode
    editorconfig
    ;; kakapo-mode ; look into for simpler indentation, https://www.reddit.com/r/haskell/comments/52rkz1/the_state_of_indentation_support_in_emacs/d7ny3at/
   )
   dotspacemacs-excluded-packages
   '(
     exec-path-from-shell ; https://github.com/syl20bnr/spacemacs/issues/2294
    )
  )
)

(defun dotspacemacs/user-init ()
    "Initialization function for user code.
It is called immediately after `dotspacemacs/init'.  You are free to put any
user code."
)

(defun dotspacemacs/user-config ()
  "Configuration function for user code.
 This function is called at the very end of Spacemacs initialization after
layers configuration. You are free to put any user code."


  ;; Haskell stuff
  (spacemacs/set-leader-keys-for-major-mode 'haskell-mode
    "msr" 'haskell-process-reload
    "msR" 'haskell-process-reload-devel-main
  )

  (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

  (setq-default flycheck-disabled-checkers '(haskell-stack-ghc))

  (add-to-list 'spacemacs-indent-sensitive-modes 'haskell-mode)


  ;; Elixir stuff
  (add-to-list 'spacemacs-indent-sensitive-modes 'elixir-mode)


  ;; Elm stuff
  (spacemacs/set-leader-keys-for-major-mode 'elm-mode
    "mff" 'elm-mode-format-buffer
    )

  (add-hook 'elm-mode-hook #'turn-off-elm-indent)

  (add-to-list 'spacemacs-indent-sensitive-modes 'elm-mode)


  ;; PHP stuff
  (setq
    php-mode-coding-style (quote psr2)
    php-template-compatibility nil
    flycheck-phpcs-standard "PSR2"
  )


  ;; Nix stuff
  (add-to-list 'spacemacs-indent-sensitive-modes 'nix-mode)


  ;; Org stuff
  (with-eval-after-load 'org
    (load-file "~/.spacemacs.d/org.el")
  )


  ;; General stuff
  (add-hook 'text-mode-hook 'auto-fill-mode)
  (add-hook 'prog-mode-hook 'auto-fill-mode)

  (global-git-commit-mode t)

  (editorconfig-mode 1)

  (add-hook 'after-change-major-mode-hook (lambda() (electric-indent-mode -1)))
)
